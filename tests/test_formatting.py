from stfu_tg import Bold, Italic, Code, Pre, Strikethrough, Underline
from stfu_tg.formatting import Spoiler, Url


def test_bold():
    assert str(Bold("Hello")) == "<b>Hello</b>"


def test_italic():
    assert str(Italic("Hello")) == "<i>Hello</i>"


def test_code():
    assert str(Code("Hello")) == "<code>Hello</code>"


def test_pre():
    assert str(Pre("Hello")) == "<pre>Hello</pre>"
    assert str(Pre("Hello", "python")) == "<pre><code class=\"language-python\">Hello</pre>"
    assert str(Pre("Hello", "javascript")) == "<pre><code class=\"language-javascript\">Hello</pre>"


def test_strikethrough():
    assert str(Strikethrough("Hello")) == "<s>Hello</s>"


def test_underline():
    assert str(Underline("Hello")) == "<u>Hello</u>"


def test_spoiler():
    assert str(Spoiler("Hello")) == "<tg-spoiler>Hello</tg-spoiler>"


def test_url():
    assert str(Url("Hello", "https://foo.bar")) == "<a href=\"https://foo.bar\">Hello</a>"


def test_html_escaping():
    assert str(Bold("<b>Hello</b>")) == "<b>&lt;b&gt;Hello&lt;/b&gt;</b>"
    assert str(Italic("<i>Hello</i>")) == "<i>&lt;i&gt;Hello&lt;/i&gt;</i>"
    assert str(Code("<code>Hello</code>")) == "<code>&lt;code&gt;Hello&lt;/code&gt;</code>"
    assert str(Pre("<pre>Hello</pre>")) == "<pre>&lt;pre&gt;Hello&lt;/pre&gt;</pre>"
    assert str(Strikethrough("<s>Hello</s>")) == "<s>&lt;s&gt;Hello&lt;/s&gt;</s>"
    assert str(Underline("<u>Hello</u>")) == "<u>&lt;u&gt;Hello&lt;/u&gt;</u>"
    assert str(Spoiler("<tg-spoiler>Hello</tg-spoiler>")) == \
           "<tg-spoiler>&lt;tg-spoiler&gt;Hello&lt;/tg-spoiler&gt;</tg-spoiler>"
    assert str(Url("<a>Hello</a>", "https://foo.bar")) == \
           "<a href=\"https://foo.bar\">&lt;a&gt;Hello&lt;/a&gt;</a>"


def test_url_with_double_quotes():
    assert str(Url("Hello", "https://foo.bar=\"Hi\"")) == \
           "<a href=\"https://foo.bar=&quot;Hi&quot;\">Hello</a>"


def test_folded():
    assert str(Bold(Italic("Hello"))) == "<b><i>Hello</i></b>"
    assert str(Italic(Bold("Hello"))) == "<i><b>Hello</b></i>"
    assert str(Url(Bold("Hello"), "https://foo.bar")) == \
           "<a href=\"https://foo.bar\"><b>Hello</b></a>"

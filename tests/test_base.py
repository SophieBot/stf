from stfu_tg import Doc


def test_doc():
    assert str(Doc("foo", "bar")) == "foo\nbar"
    assert str(Doc("foo", "bar") + "baz") == "foo\nbar\nbaz"
    assert str(Doc("foo", "bar") + Doc("foo", "bar")) == "foo\nbar\nfoo\nbar"


def test_doc_iadd_doc():
    doc = Doc("foo", "bar")
    doc += Doc("baz")
    assert str(doc) == "foo\nbar\nbaz"


def test_doc_iadd_str():
    doc = Doc("foo", "bar")
    doc += "baz"
    assert str(doc) == "foo\nbar\nbaz"


def test_doc_add():
    assert str(Doc("foo") + Doc("bar")) == "foo\nbar"

from stfu_tg import KeyValue, HList


def test_key_value():
    assert str(KeyValue("foo", "bar")) == '<b>foo</b>: bar'
    assert str(KeyValue("foo", "bar", title_bold=False)) == 'foo: bar'
    assert str(KeyValue("foo", "bar", suffix='=')) == '<b>foo</b>=bar'


def test_hlist():
    assert str(HList("foo", "bar")) == 'foo bar'
    assert str(HList("foo", "bar", prefix='-')) == '-foo -bar'
    assert str(HList("foo", "bar", divider=';')) == 'foo;bar'


def test_hlist_none():
    assert str(HList("foo", None, "bar")) == 'foo bar'

# Sophie Text Formation Utility (STFU)

A python library that built to make formatting easier.

You can read the changelog [here](https://gitlab.com/SophieBot/stf/-/tags)

## Background

This library is built to resolve many formatting issues while developing and maintain a Telegram bots.

While it's relatively simple to format the texts yourself using f-string or other similar functions (Template),
it does not resolve the main problem of translations, once you establish a translation system, you'll find that every incorrect translation could cause broken formatting of your messages or even worse, Telegram API exceptions.
The idea is that making formatting completely dedicated and independent of translations.

Another problem is painful HTML-escaping, it's horrible to ensure that every single f-string correctly escapes every single variable that could potentially have plain HTML or HTML-syntax characters.
The library escapes everything automatically.

The bonus-features, are:

1. Very simple lists generators
2. Beautiful syntax with IDE-autocompletion support
3. Ability to construct nested complicated structures such as lists in sections

# Quickstart

The library provides a bunch of elements for you to build.
You can try using the few ones, combine and see what you get.

```python
from stfu_tg import Section, KeyValue

fruits: dict[str, str] = {
    'apple': 'red',
    'qiwi:': 'green',
    'orange': 'orange'
}

sec = Section(title="Fruits' colors")
sec += "Do you remember the fruits' colors?"
for name, color in fruits.items():
    sec += KeyValue(name, color)

print(sec)
```

### A one-liner

Here, I love one-liners, especially for generators, let's refactor to use them!

```python
from stfu_tg import Section, KeyValue

fruits: dict[str, str] = {
    'apple': 'red',
    'qiwi:': 'green',
    'orange': 'orange'
}

print(Section(
    "Do you remember the fruits' colors?",
    *(KeyValue(name, color), for name, color in fruits.items),
    title="Fruits' colors"
))
```

## Advanced usage

### Creating new Elements
It's principally very simple, just make a new class based on Element and let the IDE fill all the required methods for you.
For usage example you can check STFU's sources of Elements you find the most close to what you want to create.

### Markdown

Another issue I experienced after converting everything to the STFU-elements, is that I really wanted to build a documentation generator that extracts the information from the code.
I use docusaurus, which prefers to use Markdown.
So, STFU supports now Markdown as well.
by using the element's `.to_md()` function you'd get a markdown-ready string.

#### Telegram Markdown
Please keep in mind that Telegram's standart Markdown format is a little bit different and won't work with STFU.

For example STFU will format `Bold("Foo")` to `**Bold**` and not `*Bold*` as MarkdownV2 or MarkdownV1.
Although, some Telegram libraries implements the same Markdown formatting (for example Telethon).

Lastly, STFU contains some special Elements for Markdown, such as Table. Those are not going to work with Telegram obviously.
